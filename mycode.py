import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import urllib.request #https://docs.python.org/3/library/urllib.request.html
import json #https://docs.python.org/3/library/json.html


def generate_password(length: int, complexity: int) -> str:
    s=""
    if complexity>=1 and complexity<=4:
        if complexity==1:
            for i in range(length):
                s+=random.choice(string.ascii_lowercase)
        elif complexity==2:
            if length<complexity:
                return generate_password(length, random.choice(range(1, complexity)))
            else:
                a=random.choice(range(1,length))
                for i in range(length-a):
                    s += random.choice(string.ascii_lowercase)
                for j in range(a):
                    s += random.choice(string.digits)
        elif complexity==3:
            if length<complexity:
                return generate_password(length,random.choice(range(1,complexity)))
            else:
                a = random.choice(range(1, length - 1))
                b = random.choice(range(1,length-a))
                for i in range(length-a-b):
                    s += random.choice(string.ascii_lowercase)
                for j in range(a):
                    s += random.choice(string.digits)
                for k in range(b):
                    s += random.choice(string.ascii_uppercase)
        elif complexity==4:
            if length<complexity:
                return generate_password(length,random.choice(range(1,complexity)))
            else:
                if length==4:
                    s="".join(((random.choice(string.ascii_lowercase),random.choice(string.digits),random.choice(string.ascii_uppercase),random.choice(string.punctuation))))
                else:
                    a = random.choice(range(1, length - 3))
                    b = random.choice(range(1, length - a-1))
                    e= random.choice(range(1,length-a-b))
                    for i in range(length - a - b - e):
                        s += random.choice(string.ascii_lowercase)
                    for j in range(a):
                        s += random.choice(string.digits)
                    for k in range(b):
                        s += random.choice(string.ascii_uppercase)
                    for k in range(e):
                        s += random.choice(string.punctuation)
    else:
        return "Complexity should be in range 1 to 4 included"
    return s


def check_password_level(password: str) -> int:
    lo,up,pu,di=0,0,0,0
    complexity=0
    for i in password:
        if i.islower():
            lo+=1
        elif i.isupper():
            up+=1
        elif i.isdigit():
            di+=1
        elif i in string.punctuation:
            pu+=1
    if lo==len(password):
        complexity= 1
    if lo and di:
        complexity= 2
    if lo and di and up:
        complexity=3
    if lo and di and up and pu:
        complexity=4
    if len(password)>=8 and len(password)==lo:
        complexity=2
    if len(password)>=8 and lo and di and len(password)==(lo+di):
        complexity=3
    return complexity
    
def checkmultiplescenarios():
    length=random.choice(range(5,20))
    complexity=random.choice(range(1,5))
    password=generate_password(length,complexity)
    if complexity==check_password_level(password):
        return True
    else:
        return False

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    with urllib.request.urlopen("https://randomuser.me/api") as url:
    data = json.loads(url.read().decode())
    d=(data["results"][0]['name'])
    fullname=(" ".join((d["title"],d["first"],d["last"])))
    email=(data["results"][0]['email'])

    connection=sqlite3.connect('db_path/test.db')
    curs=connection.cursor()
    curs.execute("create table if not exists users (name,email,password)")
    sql1="insert into users(Name,Email) values(?,?);"
    curs.execute(sql1,(fullname,email))
    connection.commit()
def retrievetenusers():
    connection=sqlite3.connect('db_path/test.db')
    curs=connection.cursor()
    sql="select * from users limit 10"
    curs.execute(sql)
    rows=curs.fetchall()
    for row in rows:
        na=row[0]
        length=random.choice(range(6,13))
        complexity = random.choice(range(1,5))
        password=generate_password(length,complexity)
        sql3="UPDATE users SET password=? WHERE name=?"
        curs.execute(sql3,(password,na))
    connection.commit()